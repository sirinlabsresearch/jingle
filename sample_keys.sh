#! /bin/bash

echo "Generating sample addresses..."
# m/44'/60'/0'/0/0
echo -n "0xeD432baeeD91aa9a4e4c543EfD829f5Eac9421de" > addr_1
# m/44'/60'/0'/0/1
echo -n "0x0C926cD0157CA61Af91F58B1106A8126A14A468f" > addr_2
# m/44'/60'/0'/0/2
echo -n "0xb2D342d4b54F0C37d66c5707f86C1AE703Ead5B3" > addr_3
# m/44'/60'/0'/0/3
echo -n "0xfC4617f5095556C1C669186cA1d2C9c3BDc3AB09" > addr_4
# m/44'/60'/0'/0/4
echo -n "0x65E55a86D1adE970cE0dB538940FcEe2AEaebCE7" > addr_5

for i in {1..5}; do
	echo "[$i] Creating public/private keypairs..."
	openssl ecparam -name prime256v1 -genkey -out priv_$i.pem
	openssl ec -in priv_$i.pem -pubout -out pub_$i.pem 2> /dev/null

	echo "[$i] Exported Public Key PEM:"
	openssl asn1parse -inform pem -i -dump -in pub_$i.pem
	
	echo "[$i] Creating digest..."
	openssl dgst -sha256 addr_$i | cut -f2 -d' ' | tr -d '\n' > digest_$i.bin

	echo "[$i] Signing..."
	openssl dgst -sha256 -sign priv_$i.pem addr_$i > signature_$i.der

	echo "[$i] Exported signature DER:"
	openssl asn1parse -inform der -in signature_$i.der

	echo "[$i] Verifying signature..."
	openssl dgst -sha256 -verify pub_$i.pem -signature signature_$i.der addr_$i
done

# Jingle

Jingle is a small tool to manipulate keys and signatures. It supports building a public key PEM file from a coordinates {x,y} on the curve and building a signature DER file from {r,s}.

## Installation
* Navigate to the project's directory
* Install node dependencies: `npm install`
* The script `verify.sh` will use jingle.js.

## Usage
* Make sure you are connected to a phone with ADB enabled and the wallet is open.
* Execute `verify.sh`. It parses the public key, performs signing via the wallet and verifies the signature.
* The file `message.bin` is created and contains the payload that is signed and verified (Ethereum address).
* The files `pubkey_binary.pem` and `sig.der` are generated.

Built with ❤ by SIRIN LABS

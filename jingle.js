const jose = require('node-jose');
const jwkToPem = require('jwk-to-pem');
const ArgumentParser = require('argparse').ArgumentParser;
const fs = require('fs');
const asn = require('asn1.js');

var parser = new ArgumentParser({
  version: '0.1.0',
  addHelp: true,
  description: 'Jingle - A tool for manipulating keys and signatures',
  epilog: 'Built with ❤ by SIRIN LABS (borisf@sirinlabs.com)'
});

parser.addArgument(
  [ '-c', '--coordinates' ],
  {
    help: 'Convert public key {x,y} coordinates to PEM. First x, then y.',
    nargs: 2,
    metavar: 'x/y',
  }
);
parser.addArgument(
  [ '-s', '--signature' ],
  {
    help: 'Convert signature {r,s} to DER. First r, then s.',
    nargs: 2,
    metavar: 'r/s',
  }
);

var args = parser.parseArgs();

if (args.coordinates) {
	var pubkey_binary = {
		"kty": "EC",
		"crv": "P-256",
		"x": jose.util.base64url.encode(Buffer.from(args.coordinates[0], 'hex')),
		"y": jose.util.base64url.encode(Buffer.from(args.coordinates[1], 'hex')),
	};

	var pubkey_filename = "pubkey_binary.pem";

	jose.JWK.asKey(pubkey_binary, "json")
	.then(key => {
		var pemb = jwkToPem(pubkey_binary);
		console.log(pemb);
		fs.writeFileSync(pubkey_filename, pemb)
		console.log("Public Key written to " + pubkey_filename + ".");
	})
	.catch(err => console.error(err))	
}

if (args.signature) {
	var Sig = asn.define('Sig', function() {
		this.seq().obj(
			this.key('r').int(),
			this.key('s').int()
			);
	});

	var r_buffer = Buffer.from(args.signature[0], 'hex');
	var s_buffer = Buffer.from(args.signature[1], 'hex');

	if (r_buffer[0] & 0x80)
		r_buffer = Buffer.concat([Buffer.from([0x00]), r_buffer]);
	
	if (s_buffer[0] & 0x80)
		s_buffer = Buffer.concat([Buffer.from([0x00]), s_buffer]);

	var output = Sig.encode({
		r: r_buffer,
		s: s_buffer,
	}, 'der');

	var signature_file_name = 'sig.der';
	fs.writeFileSync(signature_file_name, output);
	console.log("Signature written to " + signature_file_name + ".");
}

#! /bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

function echo_green() {
	echo -e "${GREEN}$1${NC}"
}

function echo_yellow() {
	echo -e "${YELLOW}$1${NC}"
}

echo_green "Getting Public Key from device, make sure ADB is connected. 
If this command hangs re-open the wallet and try again..."

PUBKEY=$(adb shell -x "getinfo" | grep "PUB KEY" | cut -f2 -d':' | cut -f2 -d' ' | tr -d '\n\r' | tr -d '\n')
echo_yellow "PUBKEY: ${PUBKEY}, (size = ${#PUBKEY})"

echo_green "Signing the address..."

RES=$(adb shell -x 'getsignedaddress -c 2 -p "m/44/60/0/0/0"' | grep "ADDRESS" | tr -d '\n\r' | tr -d '\n')
ADDR=$(echo $RES | cut -f2 -d' ')
SIG=$(echo $RES | cut -f4 -d' ')
echo_yellow "SIG: ${SIG}, (size = ${#SIG})"
echo_yellow "ADDR: ${ADDR}, (size = ${#ADDR})"

echo -n $ADDR > message.bin

PUBKEY_X=${PUBKEY:0:64}
PUBKEY_Y=${PUBKEY:64:64}
SIG_R=${SIG:0:64}
SIG_S=${SIG:64:64}

echo_yellow "PUBKEY_X: ${PUBKEY_X}, (size = ${#PUBKEY_X})"
echo_yellow "PUBKEY_Y: ${PUBKEY_Y}, (size = ${#PUBKEY_Y})"
echo_yellow "SIG_R: ${SIG_R}, (size = ${#SIG_R})"
echo_yellow "SIG_S: ${SIG_S}, (size = ${#SIG_S})"

if [ -f jingle.js ]; then
    JINGLE="node ./jingle.js"
else
    JINGLE="./bin/jingle-linux"
fi

echo_green "Creating public key file from {x,y}..."
$JINGLE -c $PUBKEY_X $PUBKEY_Y

echo_green "Creating signature file from {r,s}..."
$JINGLE -s $SIG_R $SIG_S

echo_green "Public Key PEM:"
openssl asn1parse -dump -inform pem -in pubkey_binary.pem

echo_green "Signature DER:"
openssl asn1parse -inform der -in sig.der

echo_green "Verifying..."
openssl dgst -sha256 -verify pubkey_binary.pem -signature sig.der message.bin

#
# If we were to create the private keys and sign...
#

# echo "Creating public/private keypair..."
# openssl ecparam -name prime256v1 -genkey -out priv.pem
# openssl ec -in priv.pem -pubout -out pub.pem 2> /dev/null

# echo "Signing..."
# openssl dgst -sha256 -sign priv.pem package.json > signature.der
